# byte slice conversion

**#GOLANG
convert byte slice to and from other types using unsafe and reflect package**

https://play.golang.org/p/TCvWw1mf4kG

![bytesliceconversion_go](https://user-images.githubusercontent.com/21541959/138377141-84d7baeb-6637-4434-917d-aebc2443905a.png)
